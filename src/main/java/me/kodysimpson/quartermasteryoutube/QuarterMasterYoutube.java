package me.kodysimpson.quartermasteryoutube;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import me.kodysimpson.quartermasteryoutube.commands.ListCommand;
import me.kodysimpson.quartermasteryoutube.commands.LockCommand;
import me.kodysimpson.quartermasteryoutube.listeners.MenuListeners;
import me.kodysimpson.quartermasteryoutube.listeners.ChestListeners;
import me.kodysimpson.quartermasteryoutube.utils.LockMenuSystem;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;


public final class QuarterMasterYoutube extends JavaPlugin {

    private MongoClient mongoClient;
    private MongoDatabase database;
    private static MongoCollection<Document> col;

    public static HashMap<Player, LockMenuSystem> lockMenuSystemHashMap = new HashMap<>();

    @Override
    public void onEnable() {
        // Plugin startup logic

        //Connect to mongodb database
        mongoClient = MongoClients.create("mongodb+srv://kody:sesame@spigotcluster-w08nt.gcp.mongodb.net/test?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("quartermaster");
        col = database.getCollection("locks");

        getCommand("lock").setExecutor(new LockCommand());
        getCommand("list").setExecutor(new ListCommand());
        Bukkit.getPluginManager().registerEvents(new MenuListeners(), this);
        Bukkit.getPluginManager().registerEvents(new ChestListeners(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static MongoCollection<Document> getDatabaseCollection() {
        return col;
    }

    public static LockMenuSystem getPlayerMenuSystem(Player p){
        LockMenuSystem lockMenuSystem = null;
        if (QuarterMasterYoutube.lockMenuSystemHashMap.containsKey(p)){
            return lockMenuSystemHashMap.get(p);
        }else{
            lockMenuSystem = new LockMenuSystem(p);
            lockMenuSystemHashMap.put(p, lockMenuSystem);

            return lockMenuSystem;
        }
    }
}
