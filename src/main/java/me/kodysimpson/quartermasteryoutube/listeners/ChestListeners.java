package me.kodysimpson.quartermasteryoutube.listeners;

import me.kodysimpson.quartermasteryoutube.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class ChestListeners implements Listener {

    @EventHandler
    public void openChestListener(PlayerInteractEvent e){
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            Block b = e.getClickedBlock();
            if (b.getType().equals(Material.CHEST)){
                //check to see if anyone has locked this chest
                if (LockUtils.isCurrentlyLocked(b)){
                    if (LockUtils.getWhoLocked(b) == e.getPlayer()){
                        e.getPlayer().sendMessage("You own this chest");
                    }else if(!(LockUtils.getWhoLocked(b) == e.getPlayer())){
                        e.setCancelled(true);
                    e.getPlayer().sendMessage(ChatColor.DARK_RED + "The chest is locked by " + ChatColor.GRAY + LockUtils.getWhoLocked(b).getName());
                    }
                }

            }
        }
    }

    @EventHandler
    public void breakChestListener(BlockBreakEvent e){
        if (e.getBlock().getType().equals(Material.CHEST)){
            if (LockUtils.isCurrentlyLocked(e.getBlock())){
                if (e.getPlayer().equals(LockUtils.getWhoLocked(e.getBlock()))){

                    LockUtils.deleteLock(e.getBlock());

                }else if(!(e.getPlayer().equals(LockUtils.getWhoLocked(e.getBlock())))){
                    e.setCancelled(true);
                    e.getPlayer().sendMessage(ChatColor.DARK_RED + "You do not own this chest. It can't be broken.");
                }
            }
        }
    }

}
