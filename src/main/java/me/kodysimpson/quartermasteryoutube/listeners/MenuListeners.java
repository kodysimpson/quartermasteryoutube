package me.kodysimpson.quartermasteryoutube.listeners;

import me.kodysimpson.quartermasteryoutube.QuarterMasterYoutube;
import me.kodysimpson.quartermasteryoutube.utils.LockMenuSystem;
import me.kodysimpson.quartermasteryoutube.utils.LockUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class MenuListeners implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();

        LockMenuSystem lockMenuSystem = QuarterMasterYoutube.getPlayerMenuSystem(p);

        if (e.getView().getTitle().equalsIgnoreCase(ChatColor.DARK_AQUA + "Locked Detected. Lock Chest?")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(null)){
                return;
            }
            if (e.getCurrentItem().getType().equals(Material.TOTEM_OF_UNDYING)){
                p.sendMessage("Creating a new lock...");
                //Create a new lock for the player
                LockUtils.createNewLock(p, lockMenuSystem.getLockToCreate());

            }else if(e.getCurrentItem().getType().equals(Material.BARRIER)){
                p.closeInventory();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.DARK_RED + "Your Locks:")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(null)){
                return;
            }else if(e.getCurrentItem().getType().equals(Material.CHEST)){

                lockMenuSystem.setLockID(lockMenuSystem.getMenu().getItem(e.getSlot()).getItemMeta().getLore().get(7));

                lockMenuSystem.showLockManagerGUI();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.GOLD + "Lock Manager")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                //go back to previous menu
                lockMenuSystem.showLocksListGUI();
            }else if(e.getCurrentItem().getType().equals(Material.WITHER_ROSE)){
                //open up confirm menu to delete lock
                lockMenuSystem.showConfirmDeleteMenu();
            }else if(e.getCurrentItem().getType().equals(Material.ARMOR_STAND)){
                //open the access manager
                lockMenuSystem.showAccessManagerMenu();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.RED + "Confirm: Delete Lock?")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showLocksListGUI();
            }else if(e.getCurrentItem().getType().equals(Material.EMERALD)){

                //Delete lock, since they confirmed yes
                LockUtils.deleteLock(lockMenuSystem.getLockID());

                p.sendMessage(ChatColor.GREEN + "Your lock(" + ChatColor.GOLD + lockMenuSystem.getLockID() + ChatColor.GREEN + ") has been deleted");

                lockMenuSystem.showLocksListGUI();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.GREEN + "Access Manager")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showLockManagerGUI();
            }else if(e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)){
                //Open player list GUI
                lockMenuSystem.showPlayersWithAccessMenu();
            }else if(e.getCurrentItem().getType().equals(Material.ENDER_EYE)){
                //open players to add menu
                lockMenuSystem.showPlayersToAddMenu();
            }else if(e.getCurrentItem().getType().equals(Material.REDSTONE_BLOCK)){
                //open players to remove menu
                lockMenuSystem.showPlayersToRemoveMenu();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.GREEN + "Choose a Player to Add:")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)){
                //Open confirm menu for adding a player
                lockMenuSystem.showConfirmAddPlayerMenu();
                lockMenuSystem.setPlayerToAdd(Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName()));
            }else if(e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showAccessManagerMenu();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "Players with Access to Lock")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showAccessManagerMenu();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "Choose a Player to Remove")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)){
                //show the confirm remove menu
                lockMenuSystem.showConfirmRemoveMenu();

                lockMenuSystem.setPlayerToRemove(Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName()));

            }else if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showAccessManagerMenu();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.GREEN + "Confirm: Add Player")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showAccessManagerMenu();
            }else if(e.getCurrentItem().getType().equals(Material.EMERALD)){
                //Add player to lock
                LockUtils.addPlayerToLock(lockMenuSystem.getLockID(), lockMenuSystem.getPlayerToAdd());

                p.sendMessage(ChatColor.GREEN + "Added " + ChatColor.YELLOW + lockMenuSystem.getPlayerToAdd().getName() + ChatColor.GREEN + " to your lock.");
                lockMenuSystem.getPlayerToAdd().sendMessage(ChatColor.YELLOW + p.getName() + ChatColor.GREEN + "has justed granted you access to one of their locks.");

                lockMenuSystem.showAccessManagerMenu();
            }
        }else if(e.getView().getTitle().equalsIgnoreCase(ChatColor.RED + "Confirm: Remove Player")){
            e.setCancelled(true);
            if (e.getCurrentItem().getType().equals(Material.BARRIER)){
                lockMenuSystem.showAccessManagerMenu();
            }else if(e.getCurrentItem().getType().equals(Material.EMERALD)){
                //Remove player from lock
                LockUtils.removePlayerFromLock(lockMenuSystem.getLockID(), lockMenuSystem.getPlayerToRemove());

                p.sendMessage(ChatColor.GRAY + "Removed " + ChatColor.YELLOW + lockMenuSystem.getPlayerToRemove().getName() + ChatColor.GRAY + " from your lock.");

                lockMenuSystem.showAccessManagerMenu();
            }
        }
    }

}
