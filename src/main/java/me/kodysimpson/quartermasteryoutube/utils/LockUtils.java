package me.kodysimpson.quartermasteryoutube.utils;

import me.kodysimpson.quartermasteryoutube.QuarterMasterYoutube;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class LockUtils {

    public static void createNewLock(Player p, Block block){

        //Document representing the new lock
        Document lock = new Document("uuid", p.getUniqueId().toString())
                .append("type", "chest")
                .append("location", new Document("x", block.getX()).append("y", block.getY()).append("z", block.getZ()))
                .append("creation-date", new Date())
                .append("access", new ArrayList<String>());
        QuarterMasterYoutube.getDatabaseCollection().insertOne(lock);
        System.out.println("New Lock Created!");

        p.closeInventory();
    }

    public static boolean isCurrentlyLocked(Block block){

        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        Document filter = new Document("location", new Document("x", x).append("y", y).append("z", z));

        if (QuarterMasterYoutube.getDatabaseCollection().countDocuments(filter) == 1){
            return true;
        }
        return false;
    }

    public static Player getWhoLocked(Block block){
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        Document filter = new Document("location", new Document("x", x).append("y", y).append("z", z));

        String uuidString = QuarterMasterYoutube.getDatabaseCollection().find(filter).first().getString("uuid");
        UUID uuid = UUID.fromString(uuidString);

        return Bukkit.getPlayer(uuid);
    }

    public static void deleteLock(Block block){
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        Document filter = new Document("location", new Document("x", x).append("y", y).append("z", z));

        //Delete the lock within the database
        QuarterMasterYoutube.getDatabaseCollection().deleteOne(filter);
        System.out.println("Lock deleted");

    }

    public static void deleteLock(String lockID){

        Document lock = LockUtils.getLock(lockID);

        QuarterMasterYoutube.getDatabaseCollection().deleteOne(lock);
    }

    public static Document getLock(String id){

        Document filter = new Document(new Document("_id", new ObjectId(id)));
        return QuarterMasterYoutube.getDatabaseCollection().find(filter).first();

    }

    public static void addPlayerToLock(String lockID, Player playerToAdd){
        Document lock = LockUtils.getLock(lockID);

        ArrayList<String> accessList = (ArrayList<String>) lock.get("access");
        accessList.add(playerToAdd.getUniqueId().toString());

        Document newDoc = new Document("access", accessList);
        Document newDoc2 = new Document("$set", newDoc);

        //Find the document from the database from the document stored in the lock
        Document filter = new Document(new Document("_id", new ObjectId(String.valueOf(lock.getObjectId("_id")))));
        QuarterMasterYoutube.getDatabaseCollection().updateOne(filter, newDoc2);

    }

    public static void removePlayerFromLock(String lockID, Player playerToRemove){
        Document lock = LockUtils.getLock(lockID);

        ArrayList<String> accessList = (ArrayList<String>) lock.get("access");
        accessList.remove(playerToRemove.getUniqueId().toString());

        Document newDoc = new Document("access", accessList);
        Document newDoc2 = new Document("$set", newDoc);

        //Find the document from the database from the document stored in the lock
        Document filter = new Document(new Document("_id", new ObjectId(String.valueOf(lock.getObjectId("_id")))));
        QuarterMasterYoutube.getDatabaseCollection().updateOne(filter, newDoc2);

    }

}
